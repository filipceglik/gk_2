//
// Created by gambo on 3/5/20.
//

#ifndef GK_II_24_H
#define GK_II_24_H

#endif //GK_II_24_H
#include <GL/glut.h> // GLUT, include glu.h and gl.h

#include <math.h> // Needed for sin, cos
#define PI 3.14159265f

// Global variables
char title[] = "Full-Screen & Windowed Mode"; // Windowed mode's title
int windowWidth = 640; // Windowed mode's width
int windowHeight = 480; // Windowed mode's height
int windowPosX = 50; // Windowed mode's top-left corner x
int windowPosY = 50; // Windowed mode's top-left corner y
GLfloat ballRadius = 0.5f; // Radius of the bouncing ball
GLfloat ballX = 0.0f; // Ball's center (x, y) position
GLfloat ballY = 0.0f;
GLfloat ballXMax, ballXMin, ballYMax, ballYMin; // Ball's center (x, y) bounds
GLfloat xSpeed = 0.02f; // Ball's speed in x and y directions
GLfloat ySpeed = 0.007f;
int refreshMillis = 30; // Refresh period in milliseconds
// Projection clipping area
GLdouble clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop;
bool fullScreenMode = true; // Full-screen or windowed mode?
bool paused = false; // Movement paused or resumed
GLfloat xSpeedSaved, ySpeedSaved; // To support resume
/* Callback handler for window re-paint event */
void display2() {
    glClear(GL_COLOR_BUFFER_BIT); // Clear the color buffer
    glMatrixMode(GL_MODELVIEW); // To operate on the model-view matrix
    glLoadIdentity(); // Reset model-view matrix
    glTranslatef(ballX, ballY, 0.0f); // Translate to (xPos, yPos)
    // Use triangular segments to form a circle
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(0.0f, 0.0f, 1.0f); // Blue
    glVertex2f(0.0f, 0.0f); // Center of circle
    int numSegments = 100;
    GLfloat angle;
    for (int i = 0; i <= numSegments; i++) { // Last vertex same as first vertex
        angle = i * 2.0f * PI / numSegments; // 360 deg for all segments
        glVertex2f(cos(angle) * ballRadius, sin(angle) * ballRadius);
    }
    glEnd();
    glutSwapBuffers(); // Swap front and back buffers (of double buffered mode)
    // Animation Control - compute the location for the next refresh
    ballX += xSpeed;
    ballY += ySpeed;
    // Check if the ball exceeds the edges
    if (ballX > ballXMax) {
        ballX = ballXMax;
        xSpeed = -xSpeed;
    } else if (ballX < ballXMin) {
        ballX = ballXMin;
        xSpeed = -xSpeed;
    }
    if (ballY > ballYMax) {
        ballY = ballYMax;
        ySpeed = -ySpeed;

    } else if (ballY < ballYMin) {
        ballY = ballYMin;
        ySpeed = -ySpeed;
    }
}
/* Call back when the windows is re-sized */
void reshape2(GLsizei width, GLsizei height) {
    // Compute aspect ratio of the new window
    if (height == 0) height = 1; // To prevent divide by 0
    GLfloat aspect = (GLfloat) width / (GLfloat) height;
    // Set the viewport to cover the new window
    glViewport(0, 0, width, height);
    // Set the aspect ratio of the clipping area to match the viewport
    glMatrixMode(GL_PROJECTION); // To operate on the Projection matrix
    glLoadIdentity(); // Reset the projection matrix
    if (width >= height) {
        clipAreaXLeft = -1.0 * aspect;
        clipAreaXRight = 1.0 * aspect;
        clipAreaYBottom = -1.0;
        clipAreaYTop = 1.0;
    } else {
        clipAreaXLeft = -1.0;
        clipAreaXRight = 1.0;
        clipAreaYBottom = -1.0 / aspect;
        clipAreaYTop = 1.0 / aspect;
    }
    gluOrtho2D(clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop);
    ballXMin = clipAreaXLeft + ballRadius;
    ballXMax = clipAreaXRight - ballRadius;
    ballYMin = clipAreaYBottom + ballRadius;
    ballYMax = clipAreaYTop - ballRadius;
}

/*
int main(int argc, char ** argv) {
    glutInit( & argc, argv); // Initialize GLUT
    glutInitDisplayMode(GLUT_DOUBLE); // Enable double buffered mode
    glutInitWindowSize(windowWidth, windowHeight); // Initial window width and height
    glutInitWindowPosition(windowPosX, windowPosY); // Initial window top-left corner (x, y)
    glutCreateWindow(title); // Create window with given title
    glutDisplayFunc(display2); // Register callback handler for window re-paint
    glutReshapeFunc(reshape2); // Register callback handler for window re-shape
    glutTimerFunc(0, Timer, 0); // First timer call immediately
    glutSpecialFunc(specialKeys); // Register callback handler for special-key event
    glutKeyboardFunc(keyboard); // Register callback handler for special-key event
    glutFullScreen(); // Put into full screen
    glutMouseFunc(mouse); // Register callback handler for mouse event
    initGL(); // Our own OpenGL initialization
    glutMainLoop(); // Enter event-processing loop
    return 0;
}*/
