#include "21.h"
#include "22.h"
#include "24.h"
/* Main function: GLUT runs as a console application starting at main() */
/*
int main(int argc, char** argv) {
    glutInit(&argc, argv);          // Initialize GLUT
    glutInitDisplayMode(GLUT_DOUBLE);  // Enable double buffered mode
    glutInitWindowSize(640, 480);   // Set the window's initial width & height - non-square
    glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
    glutCreateWindow("Animation via Idle Function");  // Create window with the given title
    glutDisplayFunc(display2);       // Register callback handler for window re-paint event
    glutReshapeFunc(reshape);       // Register callback handler for window re-size event
    glutIdleFunc(idle);             // Register callback handler if no other event
    initGL();                       // Our own OpenGL initialization
    glutMainLoop();                 // Enter the infinite event-processing loop
    return 0;
}*/

/*  //pilka
int main(int argc, char ** argv) {
    glutInit( & argc, argv); // Initialize GLUT
    glutInitDisplayMode(GLUT_DOUBLE); // Enable double buffered mode
    glutInitWindowSize(windowWidth, windowHeight); // Initial window width and height
    glutInitWindowPosition(windowPosX, windowPosY); // Initial window top-left corner (x, y)
    glutCreateWindow(title); // Create window with given title
    glutDisplayFunc(display2); // Register callback handler for window re-paint
    glutReshapeFunc(reshape2); // Register callback handler for window re-shape
    glutTimerFunc(0, Timer, 0); // First timer call immediately
    //glutSpecialFunc(specialKeys); // Register callback handler for special-key event
    //glutKeyboardFunc(keyboard); // Register callback handler for special-key event
    //glutFullScreen(); // Put into full screen
    //glutMouseFunc(mouse); // Register callback handler for mouse event
    initGL(); // Our own OpenGL initialization
    glutMainLoop(); // Enter event-processing loop
    return 0;
}
*/


int main(int argc, char** argv) {
    glutInit(&argc, argv);          // Initialize GLUT
    glutInitDisplayMode(GLUT_DOUBLE);  // Enable double buffered mode
    glutInitWindowSize(640, 480);   // Set the window's initial width & height - non-square
    glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
    glutCreateWindow("Animation via Idle Function");  // Create window with the given title
    glutDisplayFunc(display);       // Register callback handler for window re-paint event
    glutReshapeFunc(reshape);       // Register callback handler for window re-size event
    glutTimerFunc(0, Timer, 0);     // First timer call immediately
    initGL();                       // Our own OpenGL initialization
    glutMainLoop();                 // Enter the infinite event-processing loop
    return 0;
}
