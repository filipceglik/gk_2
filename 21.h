//
// Created by gambo on 3/4/20.
//

#ifndef GK_II_21_H
#define GK_II_21_H

#endif //GK_II_21_H

/*
 * GL05IdleFunc.cpp: Translation and Rotation
 * Transform primitives from their model spaces to world space (Model Transform).
 */
// #include <windows.h>  // for MS Windows
#include <GL/glut.h>  // GLUT, include glu.h and gl.h

// Global variable
GLfloat angle = 0.0f;  // Current rotational angle of the shapes

/* Initialize OpenGL Graphics */
void initGL() {
    // Set "clearing" or background color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Black and opaque
}

/* Called back when there is no other event to be handled */
void idle() {
    glutPostRedisplay();   // Post a re-paint request to activate display()
}

/* Handler for window-repaint event. Call back when the window first appears and
   whenever the window needs to be re-painted. */
void greencounterclockwise() {
    glClear(GL_COLOR_BUFFER_BIT);   // Clear the color buffer
    glMatrixMode(GL_MODELVIEW);     // To operate on Model-View matrix
    glLoadIdentity();               // Reset the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.5f, 0.f, 0.0f);    // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);                  // Each set of 4 vertices form a quad
    glColor3f(0.0f, 1.0f, 0.0f);     // Green
    glVertex2f(-0.3f, -0.3f);
    glVertex2f( 0.3f, -0.3f);
    glVertex2f( 0.3f,  0.3f);
    glVertex2f(-0.3f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    /*glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(-0.4f, -0.3f, 0.0f);   // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);
    glColor3f(0.0f, 1.0f, 0.0f); // Green
    glVertex2f(-0.3f, -0.3f);
    glVertex2f( 0.3f, -0.3f);
    glVertex2f( 0.3f,  0.3f);
    glVertex2f(-0.3f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(-0.7f, -0.5f, 0.0f);   // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);
    glColor3f(0.2f, 0.2f, 0.2f); // Dark Gray
    glVertex2f(-0.2f, -0.2f);
    glColor3f(1.0f, 1.0f, 1.0f); // White
    glVertex2f( 0.2f, -0.2f);
    glColor3f(0.2f, 0.2f, 0.2f); // Dark Gray
    glVertex2f( 0.2f,  0.2f);
    glColor3f(1.0f, 1.0f, 1.0f); // White
    glVertex2f(-0.2f,  0.2f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.4f, -0.3f, 0.0f);    // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_TRIANGLES);
    glColor3f(0.0f, 0.0f, 1.0f); // Blue
    glVertex2f(-0.3f, -0.2f);
    glVertex2f( 0.3f, -0.2f);
    glVertex2f( 0.0f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.6f, -0.6f, 0.0f);    // Translate
    glRotatef(180.0f + angle, 0.0f, 0.0f, 1.0f); // Rotate 180+angle degree
    glBegin(GL_TRIANGLES);
    glColor3f(1.0f, 0.0f, 0.0f); // Red
    glVertex2f(-0.3f, -0.2f);
    glColor3f(0.0f, 1.0f, 0.0f); // Green
    glVertex2f( 0.3f, -0.2f);
    glColor3f(0.0f, 0.0f, 1.0f); // Blue
    glVertex2f( 0.0f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.5f, 0.4f, 0.0f);     // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_POLYGON);
    glColor3f(1.0f, 1.0f, 0.0f); // Yellow
    glVertex2f(-0.1f, -0.2f);
    glVertex2f( 0.1f, -0.2f);
    glVertex2f( 0.2f,  0.0f);
    glVertex2f( 0.1f,  0.2f);
    glVertex2f(-0.1f,  0.2f);
    glVertex2f(-0.2f,  0.0f);
    glEnd();
    glPopMatrix(); */                     // Restore the model-view matrix

    glutSwapBuffers();   // Double buffered - swap the front and back buffers

    // Change the rotational angle after each display()
    angle += 0.9f;
}

void greenclockwise() {
    glClear(GL_COLOR_BUFFER_BIT);   // Clear the color buffer
    glMatrixMode(GL_MODELVIEW);     // To operate on Model-View matrix
    glLoadIdentity();               // Reset the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.5f, 0.f, 0.0f);    // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);                  // Each set of 4 vertices form a quad
    glColor3f(0.0f, 1.0f, 0.0f);     // Green
    glVertex2f(-0.3f, -0.3f);
    glVertex2f( 0.3f, -0.3f);
    glVertex2f( 0.3f,  0.3f);
    glVertex2f(-0.3f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glutSwapBuffers();   // Double buffered - swap the front and back buffers

    // Change the rotational angle after each display()
    angle -= 0.2f;
}

void greeny() {
    glClear(GL_COLOR_BUFFER_BIT);   // Clear the color buffer
    glMatrixMode(GL_MODELVIEW);     // To operate on Model-View matrix
    glLoadIdentity();               // Reset the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.5f, 0.f, 0.0f);    // Translate
    glRotatef(angle, 0.0f, 1.0f, 0.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);                  // Each set of 4 vertices form a quad
    glColor3f(0.0f, 1.0f, 0.0f);     // Green
    glVertex2f(-0.3f, -0.3f);
    glVertex2f( 0.3f, -0.3f);
    glVertex2f( 0.3f,  0.3f);
    glVertex2f(-0.3f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glutSwapBuffers();   // Double buffered - swap the front and back buffers

    // Change the rotational angle after each display()
    angle += 0.5f;
}

void greenx() {
    glClear(GL_COLOR_BUFFER_BIT);   // Clear the color buffer
    glMatrixMode(GL_MODELVIEW);     // To operate on Model-View matrix
    glLoadIdentity();               // Reset the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.5f, 0.f, 0.0f);    // Translate
    glRotatef(angle, 1.0f, 0.0f, 0.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);                  // Each set of 4 vertices form a quad
    glColor3f(0.0f, 1.0f, 0.0f);     // Green
    glVertex2f(-0.3f, -0.3f);
    glVertex2f( 0.3f, -0.3f);
    glVertex2f( 0.3f,  0.3f);
    glVertex2f(-0.3f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glutSwapBuffers();   // Double buffered - swap the front and back buffers

    // Change the rotational angle after each display()
    angle += 0.2f;
}

/* Handler for window re-size event. Called back when the window first appears and
   whenever the window is re-sized with its new width and height */
void reshape(GLsizei width, GLsizei height) {  // GLsizei for non-negative integer
    // Compute aspect ratio of the new window
    if (height == 0) height = 1;                // To prevent divide by 0
    GLfloat aspect = (GLfloat)width / (GLfloat)height;

    // Set the viewport to cover the new window
    glViewport(0, 0, width, height);

    // Set the aspect ratio of the clipping area to match the viewport
    glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
    glLoadIdentity();
    if (width >= height) {
        // aspect >= 1, set the height from -1 to 1, with larger width
        gluOrtho2D(-1.0 * aspect, 1.0 * aspect, -1.0, 1.0);
    } else {
        // aspect < 1, set the width to -1 to 1, with larger height
        gluOrtho2D(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect);
    }
}
