//
// Created by gambo on 3/4/20.
//

#ifndef GK_II_22_H
#define GK_II_22_H

#endif //GK_II_22_H

/*
 * GL06TimerFunc.cpp: Translation and Rotation
 * Transform primitives from their model spaces to world space (Model Transform).
 */
//#include <windows.h>  // for MS Windows

#include <stdlib.h>
#include <GL/glut.h>  // GLUT, include glu.h and gl.h

// global variable
int refreshMills = 30; // refresh interval in milliseconds

/* Called back when timer expired */
void Timer(int value) {
    glutPostRedisplay();      // Post re-paint request to activate display()
    glutTimerFunc(refreshMills, Timer, 0); // next Timer call milliseconds later
}

/* Handler for window-repaint event. Call back when the window first appears and
   whenever the window needs to be re-painted. */
void display() {
    glClear(GL_COLOR_BUFFER_BIT);   // Clear the color buffer
    glMatrixMode(GL_MODELVIEW);     // To operate on Model-View matrix
    glLoadIdentity();               // Reset the model-view matrix

    /*glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(-0.5f, 0.4f, 0.0f);    // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);                  // Each set of 4 vertices form a quad
    glColor3f(1.0f, 0.0f, 0.0f);     // Red
    glVertex2f(-0.3f, -0.3f);
    glVertex2f( 0.3f, -0.3f);
    glVertex2f( 0.3f,  0.3f);
    glVertex2f(-0.3f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(-0.4f, -0.3f, 0.0f);   // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);
    glColor3f(0.0f, 1.0f, 0.0f); // Green
    glVertex2f(-0.3f, -0.3f);
    glVertex2f( 0.3f, -0.3f);
    glVertex2f( 0.3f,  0.3f);
    glVertex2f(-0.3f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(-0.7f, -0.5f, 0.0f);   // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_QUADS);
    glColor3f(0.2f, 0.2f, 0.2f); // Dark Gray
    glVertex2f(-0.2f, -0.2f);
    glColor3f(1.0f, 1.0f, 1.0f); // White
    glVertex2f( 0.2f, -0.2f);
    glColor3f(0.2f, 0.2f, 0.2f); // Dark Gray
    glVertex2f( 0.2f,  0.2f);
    glColor3f(1.0f, 1.0f, 1.0f); // White
    glVertex2f(-0.2f,  0.2f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.4f, -0.3f, 0.0f);    // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_TRIANGLES);
    glColor3f(0.0f, 0.0f, 1.0f); // Blue
    glVertex2f(-0.3f, -0.2f);
    glVertex2f( 0.3f, -0.2f);
    glVertex2f( 0.0f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.6f, -0.6f, 0.0f);    // Translate
    glRotatef(180.0f + angle, 0.0f, 0.0f, 1.0f); // Rotate 180+angle degree
    glBegin(GL_TRIANGLES);
    glColor3f(1.0f, 0.0f, 0.0f); // Red
    glVertex2f(-0.3f, -0.2f);
    glColor3f(0.0f, 1.0f, 0.0f); // Green
    glVertex2f( 0.3f, -0.2f);
    glColor3f(0.0f, 0.0f, 1.0f); // Blue
    glVertex2f( 0.0f,  0.3f);
    glEnd();
    glPopMatrix();                      // Restore the model-view matrix

    glPushMatrix();                     // Save model-view matrix setting
    glTranslatef(0.5f, 0.4f, 0.0f);     // Translate
    glRotatef(angle, 0.0f, 0.0f, 1.0f); // rotate by angle in degrees
    glBegin(GL_POLYGON);
    glColor3f(1.0f, 1.0f, 0.0f); // Yellow
    glVertex2f(-0.1f, -0.2f);
    glVertex2f( 0.1f, -0.2f);
    glVertex2f( 0.2f,  0.0f);
    glVertex2f( 0.1f,  0.2f);
    glVertex2f(-0.1f,  0.2f);
    glVertex2f(-0.2f,  0.0f);
    glEnd();
    glPopMatrix();  */                    // Restore the model-view matrix

    glPushMatrix();
    glTranslatef(0.5f, 0.4f, 0.0f);
    glRotatef(angle, 1.0f,0.0f,0.0f);   //wg osi y
    glRotatef(angle, 0.0f,1.0f,0.0f);
    glRotatef(angle, 0.0f,0.0f,1.0f);
    glBegin(GL_LINES);
    glColor3f(1.0f,0.0f,0.0f);
    glVertex2f(-0.3f,0.3f);
    glVertex2f(0.3f,0.3f);
    glEnd();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-0.5f, 0.4f, 0.0f);
    glRotatef(angle, 1.0f,0.0f,0.0f);   //wg osi y
    glRotatef(angle, 0.0f,1.0f,0.0f);
    glRotatef(angle, 0.0f,0.0f,1.0f);
    glBegin(GL_LINES);
    glColor3f(1.0f,0.0f,0.0f);
    glVertex2f(-0.3f,0.3f);
    glVertex2f(0.3f,0.3f);
    glEnd();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-0.3f,-0.3f, 0.0f);
    glBegin(GL_TRIANGLES);
    glColor3f(0.0f,1.0f,0.0f);
    glVertex2f(0.1f,0.1f);
    glVertex2f(0.1f,-0.1f);
    glVertex2f(-0.1f,0.05f);
    glEnd();

    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.3f,-0.3f, 0.0f);
    glBegin(GL_TRIANGLES);
    glColor3f(0.0f,0.0f,1.0f);
    glVertex2f(0.4f,0.4f);
    glVertex2f(0.4f,-0.4f);
    glVertex2f(-0.4f,0.2f);
    glEnd();

    glPopMatrix();

    glPushMatrix();
    glBegin(GL_POLYGON);
    glColor3ub(123,123,123);
    glVertex2f(0.40f,0.2f);
    glVertex2f(0.6f,0.2f);
    glVertex2f(0.7f,0.4f);
    glVertex2f(0.6f,0.6f);
    glVertex2f(0.4f,0.6f);
    glEnd();

    glPopMatrix();

    glutSwapBuffers();   // Double buffered - swap the front and back buffers

    // Change the rotational angle after each display()
    angle += 2.0f;
}



/* Main function: GLUT runs as a console application starting at main() *//*

int main(int argc, char** argv) {
    glutInit(&argc, argv);          // Initialize GLUT
    glutInitDisplayMode(GLUT_DOUBLE);  // Enable double buffered mode
    glutInitWindowSize(640, 480);   // Set the window's initial width & height - non-square
    glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
    glutCreateWindow("Animation via Idle Function");  // Create window with the given title
    glutDisplayFunc(display2);       // Register callback handler for window re-paint event
    glutReshapeFunc(reshape);       // Register callback handler for window re-size event
    glutTimerFunc(0, Timer, 0);     // First timer call immediately
    initGL();                       // Our own OpenGL initialization
    glutMainLoop();                 // Enter the infinite event-processing loop
    return 0;
}*/
